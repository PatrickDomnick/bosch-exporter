package main

import (
	"fmt"
	"net/http"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"gitlab.com/PatrickDomnick/bosch-exporter/pkg/bosch"
	"gitlab.com/PatrickDomnick/bosch-exporter/pkg/collector"
	"golang.org/x/time/rate"
)

func main() {
	// Read Config
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath(".")
	if err := viper.ReadInConfig(); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			log.Warn("Config File not found")
		} else {
			log.Fatal(fmt.Errorf("fatal error config file: %w", err))
		}
	}

	// Configure Logger
	log.SetReportCaller(true)
	viper.SetDefault("logLevel", "warn")
	logLevel := viper.Get("logLevel")
	switch logLevel {
	case "debug":
		log.SetLevel(log.DebugLevel)
	case "info":
		log.SetLevel(log.InfoLevel)
	case "warn":
		log.SetLevel(log.WarnLevel)
	case "error":
		log.SetLevel(log.ErrorLevel)
	}
	log.Println(log.IsLevelEnabled(log.DebugLevel))

	// Configure RateLimited HTTP Client
	viper.SetDefault("rateLimiter", 10)
	rateLimiter := viper.GetInt("rateLimiter")
	rl := rate.NewLimiter(rate.Every(50*time.Millisecond), rateLimiter) // 200 request every 1 seconds
	hostIp := fmt.Sprintf("https://%s:8444/smarthome/devices", viper.GetString("HostIp"))
	viper.SetDefault("certFile", "client-cert.pem")
	certFile := viper.GetString("certFile")
	viper.SetDefault("keyFile", "client-key.pem")
	keyFile := viper.GetString("keyFile")
	bosch.NewClient(rl, hostIp, certFile, keyFile)

	// Check API Connection and get all Devices
	log.Info("Connect to Bosch Smart Home and get all Devices")
	rooms := bosch.GetRooms()
	log.Info(rooms)
	devices := bosch.GetDevices(rooms)
	log.Info(devices)

	// Prepare Collector Array
	shutterDevices := []bosch.BoschDevice{}
	contactDevices := []bosch.BoschDevice{}
	waterDevices := []bosch.BoschDevice{}
	powerSwitchDevices := []bosch.BoschDevice{}
	powerMeterDevices := []bosch.BoschDevice{}
	temperatureDevices := []bosch.BoschDevice{}
	humidityDevices := []bosch.BoschDevice{}

	// Categorizing Devices
	log.Info(fmt.Sprintf("Found devices: %d", len(devices)))
	for _, device := range devices {
		log.Debug(device)
		// Service Device Types
		for _, dsi := range device.DeviceServiceIds {
			switch dsi {
			case "ShutterControl":
				shutterDevices = append(shutterDevices, device)
			case "ShutterContact":
				contactDevices = append(contactDevices, device)
			case "WaterLeakageSensor":
				waterDevices = append(waterDevices, device)
			case "PowerMeter":
				powerMeterDevices = append(powerMeterDevices, device)
			case "PowerSwitch":
				powerSwitchDevices = append(powerSwitchDevices, device)
			case "TemperatureLevel":
				temperatureDevices = append(temperatureDevices, device)
			case "HumidityLevel":
				humidityDevices = append(humidityDevices, device)
			}
		}
	}

	// Create a new instance of the collector and register it with the prometheus client.
	log.Info("Starting the Collector")
	collectorDevices := collector.NewBoschDevice(rooms)
	collectorShutters := collector.NewBoschShutter(shutterDevices)
	collectorContact := collector.NewBoschContact(contactDevices)
	collectorWater := collector.NewBoschWater(waterDevices)
	collectorPowerMeter := collector.NewBoschPowerMeter(powerMeterDevices)
	collectorPowerSwitch := collector.NewBoschPowerSwitch(powerSwitchDevices)
	collectorTemperature := collector.NewBoschTemperature(temperatureDevices)
	collectorHumidity := collector.NewBoschHumidity(humidityDevices)
	prometheus.MustRegister(collectorDevices, collectorShutters, collectorContact, collectorWater, collectorPowerMeter, collectorPowerSwitch, collectorTemperature, collectorHumidity)

	// This section will start the HTTP server and expose any metrics on the /metrics endpoint.
	http.Handle("/metrics", promhttp.Handler())
	log.Info("Beginning to serve on port :8080")
	log.Fatal(http.ListenAndServe(":8080", nil))
}
