FROM golang:1.16.6-alpine3.14 AS build

ADD . /go/src/gitlab.com/PatrickDomnick/bosch-exporter

RUN cd /go/src/gitlab.com/PatrickDomnick/bosch-exporter && \
    go get -d -v ./... && \
    CGO_ENABLED=0 go build -o /go/bin/bosch-exporter

FROM scratch

LABEL maintainer="patrickfdomnick@gmail.com"

COPY --from=build /go/bin/bosch-exporter /

EXPOSE 8080/tcp
ENTRYPOINT ["/bosch-exporter"]
