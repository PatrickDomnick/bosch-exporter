package bosch

import (
	"fmt"

	log "github.com/sirupsen/logrus"
)

type boschHumidity struct {
	Type     string  `json:"@type"`
	Humidity float64 `json:"humidity"`
}

func GetHumidity(id string) boschHumidity {
	// Devices Endpoint
	humidityApi := fmt.Sprintf(humidityUrl, id)
	log.Info(fmt.Sprintf("Checking the following URL: '%s'", humidityApi))

	humidityApiResponse := boschHumidity{}

	// Execute the Bosch API Call
	err := getJSON(humidityApi, &humidityApiResponse)

	// HTTP Error
	if err != nil {
		log.Fatal(err.Error())
	}

	return humidityApiResponse
}
