package bosch

import (
	"fmt"

	log "github.com/sirupsen/logrus"
)

type boschPowerSwitchState struct {
	Type                  string  `json:"@type"`
	SwitchState           string  `json:"switchState"`
	AutomaticPowerOffTime float64 `json:"automaticPowerOffTime"`
}

func GetPowerSwitchState(id string) boschPowerSwitchState {
	// Devices Endpoint
	powerSwitchStateApi := fmt.Sprintf(powerSwitchStateUrl, id)
	log.Info(fmt.Sprintf("Checking the following URL: '%s'", powerSwitchStateApi))

	powerSwitchStateApiResponse := boschPowerSwitchState{}

	// Execute the Bosch API Call
	err := getJSON(powerSwitchStateApi, &powerSwitchStateApiResponse)

	// HTTP Error
	if err != nil {
		log.Fatal(err.Error())
	}

	return powerSwitchStateApiResponse
}
