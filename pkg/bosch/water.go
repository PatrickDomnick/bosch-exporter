package bosch

import (
	"fmt"

	log "github.com/sirupsen/logrus"
)

type boschWaterState struct {
	Type  string `json:"@type"`
	Value string `json:"state"`
}

func GetWaterState(id string) boschWaterState {
	// Devices Endpoint
	waterStateApi := fmt.Sprintf(waterStateUrl, id)
	log.Info(fmt.Sprintf("Checking the following URL: '%s'", waterStateApi))

	waterStateApiResponse := boschWaterState{}

	// Execute the Bosch API Call
	err := getJSON(waterStateApi, &waterStateApiResponse)

	// HTTP Error
	if err != nil {
		log.Fatal(err.Error())
	}

	return waterStateApiResponse
}
