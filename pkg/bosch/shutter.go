package bosch

import (
	"fmt"

	log "github.com/sirupsen/logrus"
)

type boschShutterState struct {
	Type                 string `json:"@type"`
	Calibrated           bool   `json:"calibrated"`
	ReferenceMovingTimes struct {
		MovingTimeTopToBottomInMillis int `json:"movingTimeTopToBottomInMillis"`
		MovingTimeBottomToTopInMillis int `json:"movingTimeBottomToTopInMillis"`
	} `json:"referenceMovingTimes"`
	Level                      float64 `json:"level"`
	OperationState             string  `json:"operationState"`
	EndPositionAutoDetect      bool    `json:"endPositionAutoDetect"`
	EndPositionSupported       bool    `json:"endPositionSupported"`
	DelayCompensationTime      float64 `json:"delayCompensationTime"`
	DelayCompensationSupported bool    `json:"delayCompensationSupported"`
	AutomaticDelayCompensation bool    `json:"automaticDelayCompensation"`
	SlatsRunningTimeInMillis   int     `json:"slatsRunningTimeInMillis"`
}

func GetShutterState(id string) boschShutterState {
	// Devices Endpoint
	shutterStateApi := fmt.Sprintf(shutterStateUrl, id)
	log.Info(fmt.Sprintf("Checking the following URL: '%s'", shutterStateApi))

	shutterStateApiResponse := boschShutterState{}

	// Execute the Bosch API Call
	err := getJSON(shutterStateApi, &shutterStateApiResponse)

	// HTTP Error
	if err != nil {
		log.Fatal(err.Error())
	}

	return shutterStateApiResponse
}
