package bosch

import (
	"context"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"
	"golang.org/x/time/rate"
)

var Client RLHTTPClient

//RLHTTPClient Rate Limited HTTP Client
type RLHTTPClient struct {
	host        string
	client      *http.Client
	Ratelimiter *rate.Limiter
}

//Do dispatches the HTTP request to the network
func (c *RLHTTPClient) Do(req *http.Request) (*http.Response, error) {
	// Comment out the below 5 lines to turn off ratelimiting
	ctx := context.Background()
	err := c.Ratelimiter.Wait(ctx) // This is a blocking call. Honors the rate limit
	if err != nil {
		return nil, err
	}
	resp, err := c.client.Do(req)
	if err != nil {
		return nil, err
	}
	return resp, nil
}

//NewClient return http client with a ratelimiter
func NewClient(rl *rate.Limiter, host string, certFile string, keyFile string) error {
	// Load client cert
	log.Debug("Load Certificate Key Pair")
	certPair, err := tls.LoadX509KeyPair(certFile, keyFile)
	if err != nil {
		log.Fatal(err)
		return err
	} else {
		log.Debug("Successfully loaded Certificate Key Pair")
	}
	// Create HTTP Client
	client := &http.Client{
		Timeout: time.Second * 10,
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: true,
				Certificates:       []tls.Certificate{certPair},
			},
		},
	}
	Client = RLHTTPClient{
		host:        host,
		client:      client,
		Ratelimiter: rl,
	}
	return nil
}

// Convert JSON to Object
func getJSON(url string, target interface{}) error {
	log.Debug("Preparing Request")
	if url == "rooms" { // I am sorry OK?!
		url = strings.Replace(Client.host, "devices", url, 1)
	} else {
		url = fmt.Sprintf("%s/%s", Client.host, url)
	}
	log.Info(url)
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		log.Error(err)
	}
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("api-version", "2.5")

	log.Debug("Executing the Request")
	r, err := Client.Do(req)
	if err != nil {
		log.Print(r)
		log.Error(err)
	} else {
		log.Debug("Successfully executed the Request")
	}
	defer r.Body.Close()

	return json.NewDecoder(r.Body).Decode(target)
}
