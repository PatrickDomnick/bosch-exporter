package bosch

import (
	log "github.com/sirupsen/logrus"
)

type BoschRoom struct {
	Type   string `json:"@type"`
	ID     string `json:"id"`
	IconID string `json:"iconId"`
	Name   string `json:"name"`
}

func GetRooms() []BoschRoom {
	// Devices Endpoint
	log.Info("Getting all Rooms")

	roomsApiResponse := []BoschRoom{}

	// Execute the Bosch API Call
	err := getJSON("rooms", &roomsApiResponse)

	// HTTP Error
	if err != nil {
		log.Fatal(err.Error())
	}

	return roomsApiResponse
}
