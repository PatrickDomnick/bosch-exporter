package bosch

const shutterStateUrl = "%s/services/ShutterControl/state"
const contactStateUrl = "%s/services/ShutterContact/state"
const waterStateUrl = "%s/services/WaterLeakageSensor/state"
const powerMeterStateUrl = "%s/services/PowerMeter/state"
const powerSwitchStateUrl = "%s/services/PowerSwitch/state"
const temperatureUrl = "%s/services/TemperatureLevel/state"
const humidityUrl = "%s/services/HumidityLevel/state"
