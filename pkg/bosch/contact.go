package bosch

import (
	"fmt"

	log "github.com/sirupsen/logrus"
)

type boschContactState struct {
	Type  string `json:"@type"`
	Value string `json:"value"`
}

func GetContactState(id string) boschContactState {
	// Devices Endpoint
	contactStateApi := fmt.Sprintf(contactStateUrl, id)
	log.Info(fmt.Sprintf("Checking the following URL: '%s'", contactStateApi))

	contactStateApiResponse := boschContactState{}

	// Execute the Bosch API Call
	err := getJSON(contactStateApi, &contactStateApiResponse)

	// HTTP Error
	if err != nil {
		log.Fatal(err.Error())
	}

	return contactStateApiResponse
}
