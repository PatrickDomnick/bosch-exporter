package bosch

import (
	log "github.com/sirupsen/logrus"
)

type BoschDevice struct {
	Type             string   `json:"@type"`
	RootDeviceID     string   `json:"rootDeviceId"`
	ID               string   `json:"id"`
	DeviceServiceIds []string `json:"deviceServiceIds"`
	Manufacturer     string   `json:"manufacturer"`
	RoomID           string   `json:"roomId"`
	RoomName         string
	DeviceModel      string   `json:"deviceModel"`
	Profile          string   `json:"profile"`
	IconID           string   `json:"iconId"`
	Name             string   `json:"name"`
	Status           string   `json:"status"`
	ParentDeviceID   string   `json:"parentDeviceId"`
	ChildDeviceIds   []string `json:"childDeviceIds"`
}

func GetDevices(rooms []BoschRoom) []BoschDevice {
	// Devices Endpoint
	log.Info("Getting all Devices")

	devicesApiResponse := []BoschDevice{}

	// Execute the Bosch API Call
	err := getJSON("", &devicesApiResponse)

	// HTTP Error
	if err != nil {
		log.Fatal(err.Error())
	}

	// Populate Room Names
	deviceRooms := []BoschDevice{}
	for _, device := range devicesApiResponse {
		for _, room := range rooms {
			if device.RoomID == room.ID {
				device.RoomName = room.Name
			}
		}
		deviceRooms = append(deviceRooms, device)
	}

	return deviceRooms
}
