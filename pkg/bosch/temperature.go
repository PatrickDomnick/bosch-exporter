package bosch

import (
	"fmt"

	log "github.com/sirupsen/logrus"
)

type boschTemperature struct {
	Type        string  `json:"@type"`
	Temperature float64 `json:"temperature"`
}

func GetTemperature(id string) boschTemperature {
	// Devices Endpoint
	temperatureApi := fmt.Sprintf(temperatureUrl, id)
	log.Info(fmt.Sprintf("Checking the following URL: '%s'", temperatureApi))

	temperatureApiResponse := boschTemperature{}

	// Execute the Bosch API Call
	err := getJSON(temperatureApi, &temperatureApiResponse)

	// HTTP Error
	if err != nil {
		log.Fatal(err.Error())
	}

	return temperatureApiResponse
}
