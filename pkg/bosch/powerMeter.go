package bosch

import (
	"fmt"

	log "github.com/sirupsen/logrus"
)

type boschPowerMeterState struct {
	Type              string  `json:"@type"`
	PowerConsumption  float64 `json:"powerConsumption"`
	EnergyConsumption float64 `json:"energyConsumption"`
}

func GetPowerMeterState(id string) boschPowerMeterState {
	// Devices Endpoint
	powerMeterStateApi := fmt.Sprintf(powerMeterStateUrl, id)
	log.Info(fmt.Sprintf("Checking the following URL: '%s'", powerMeterStateApi))

	powermeterStateApiResponse := boschPowerMeterState{}

	// Execute the Bosch API Call
	err := getJSON(powerMeterStateApi, &powermeterStateApiResponse)

	// HTTP Error
	if err != nil {
		log.Fatal(err.Error())
	}

	return powermeterStateApiResponse
}
