package collector

import (
	"github.com/prometheus/client_golang/prometheus"
	log "github.com/sirupsen/logrus"
	"gitlab.com/PatrickDomnick/bosch-exporter/pkg/bosch"
)

type BoschPowerSwitch struct {
	BoschDevices []bosch.BoschDevice
	On           *prometheus.Desc
}

func NewBoschPowerSwitch(boschDevices []bosch.BoschDevice) *BoschPowerSwitch {
	return &BoschPowerSwitch{
		BoschDevices: boschDevices,
		On:           prometheus.NewDesc(powerSwitchState, "power switch on state", powerSwitchLabels, nil),
	}
}

func (s *BoschPowerSwitch) Describe(c chan<- *prometheus.Desc) {
	prometheus.DescribeByCollect(s, c)
}

func (s *BoschPowerSwitch) Collect(c chan<- prometheus.Metric) {
	log.Info("Collecting Metrics for this power switch")
	// Level
	for _, bd := range s.BoschDevices {
		boschPowerSwitch := bosch.GetPowerSwitchState(bd.ID)
		// Operation State
		state := float64(0)
		if boschPowerSwitch.SwitchState == "ON" {
			state = float64(1)
		}
		c <- prometheus.MustNewConstMetric(s.On, prometheus.GaugeValue, state, bd.ID, bd.Manufacturer, bd.RoomID, bd.Name, bd.RoomName)
	}
}
