package collector

import (
	"github.com/prometheus/client_golang/prometheus"
	log "github.com/sirupsen/logrus"
	"gitlab.com/PatrickDomnick/bosch-exporter/pkg/bosch"
)

type BoschContact struct {
	BoschDevices []bosch.BoschDevice
	Closed       *prometheus.Desc
}

func NewBoschContact(boschDevices []bosch.BoschDevice) *BoschContact {
	return &BoschContact{
		BoschDevices: boschDevices,
		Closed:       prometheus.NewDesc(contactState, "closed contact state", contactLabels, nil),
	}
}

func (s *BoschContact) Describe(c chan<- *prometheus.Desc) {
	prometheus.DescribeByCollect(s, c)
}

func (s *BoschContact) Collect(c chan<- prometheus.Metric) {
	log.Info("Collecting Metrics for this contact")
	// Level
	for _, bd := range s.BoschDevices {
		boschContact := bosch.GetContactState(bd.ID)
		// Operation State
		state := float64(0)
		if boschContact.Value == "CLOSED" {
			state = float64(1)
		}
		c <- prometheus.MustNewConstMetric(s.Closed, prometheus.GaugeValue, state, bd.ID, bd.Manufacturer, bd.RoomID, bd.Name, bd.RoomName)
	}
}
