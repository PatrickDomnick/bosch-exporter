package collector

const deviceState = "bosch_device_state"
const shutterStateLevel = "bosch_shutter_state_level"
const shutterStateOperation = "bosch_shutter_state_operation"
const contactState = "bosch_contact_closed"
const waterState = "bosch_water_leakage"
const powerSwitchState = "bosch_power_switch_state"
const powerMeterPowerConsumption = "bosch_power_meter_power_consumption"
const powerMeterEnergyConsumption = "bosch_power_meter_energy_consumption"
const humidity = "bosch_humidity"
const temperature = "bosch_temperature"

var defaultLabels = []string{"id", "manufacturer", "roomId", "name", "roomName"}
var deviceLabels = defaultLabels
var shutterLabels = defaultLabels
var contactLabels = defaultLabels
var waterLabels = defaultLabels
var powerSwitchLabels = defaultLabels
var powerMeterLabels = defaultLabels
var humidityLabels = defaultLabels
var temperatureLabels = defaultLabels
