package collector

import (
	"github.com/prometheus/client_golang/prometheus"
	log "github.com/sirupsen/logrus"
	"gitlab.com/PatrickDomnick/bosch-exporter/pkg/bosch"
)

type BoschPowerMeter struct {
	BoschDevices      []bosch.BoschDevice
	PowerConsumption  *prometheus.Desc
	EnergyConsumption *prometheus.Desc
}

func NewBoschPowerMeter(boschDevices []bosch.BoschDevice) *BoschPowerMeter {
	return &BoschPowerMeter{
		BoschDevices:      boschDevices,
		PowerConsumption:  prometheus.NewDesc(powerMeterPowerConsumption, "power consumption in W", powerMeterLabels, nil),
		EnergyConsumption: prometheus.NewDesc(powerMeterEnergyConsumption, "all time power consumption in kWh", powerMeterLabels, nil),
	}
}

func (s *BoschPowerMeter) Describe(c chan<- *prometheus.Desc) {
	prometheus.DescribeByCollect(s, c)
}

func (s *BoschPowerMeter) Collect(c chan<- prometheus.Metric) {
	log.Info("Collecting Metrics for this power meter")
	// Level
	for _, bd := range s.BoschDevices {
		boschPowerMeter := bosch.GetPowerMeterState(bd.ID)
		c <- prometheus.MustNewConstMetric(s.PowerConsumption, prometheus.GaugeValue, boschPowerMeter.PowerConsumption, bd.ID, bd.Manufacturer, bd.RoomID, bd.Name, bd.RoomName)
		c <- prometheus.MustNewConstMetric(s.EnergyConsumption, prometheus.CounterValue, boschPowerMeter.EnergyConsumption, bd.ID, bd.Manufacturer, bd.RoomID, bd.Name, bd.RoomName)
	}
}
