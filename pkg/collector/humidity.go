package collector

import (
	"github.com/prometheus/client_golang/prometheus"
	log "github.com/sirupsen/logrus"
	"gitlab.com/PatrickDomnick/bosch-exporter/pkg/bosch"
)

type BoschHumidity struct {
	BoschDevices []bosch.BoschDevice
	Humidity     *prometheus.Desc
}

func NewBoschHumidity(boschDevices []bosch.BoschDevice) *BoschHumidity {
	return &BoschHumidity{
		BoschDevices: boschDevices,
		Humidity:     prometheus.NewDesc(humidity, "humidity in percentage", humidityLabels, nil),
	}
}

func (s *BoschHumidity) Describe(c chan<- *prometheus.Desc) {
	prometheus.DescribeByCollect(s, c)
}

func (s *BoschHumidity) Collect(c chan<- prometheus.Metric) {
	log.Info("Collecting Metrics for this humidity sensor")
	for _, bd := range s.BoschDevices {
		boschHumidity := bosch.GetHumidity(bd.ID)
		c <- prometheus.MustNewConstMetric(s.Humidity, prometheus.GaugeValue, boschHumidity.Humidity, bd.ID, bd.Manufacturer, bd.RoomID, bd.Name, bd.RoomName)
	}
}
