package collector

import (
	"github.com/prometheus/client_golang/prometheus"
	log "github.com/sirupsen/logrus"
	"gitlab.com/PatrickDomnick/bosch-exporter/pkg/bosch"
)

type BoschTemperature struct {
	BoschDevices []bosch.BoschDevice
	Temperature  *prometheus.Desc
}

func NewBoschTemperature(boschDevices []bosch.BoschDevice) *BoschTemperature {
	return &BoschTemperature{
		BoschDevices: boschDevices,
		Temperature:  prometheus.NewDesc(temperature, "temperature in celsius", temperatureLabels, nil),
	}
}

func (s *BoschTemperature) Describe(c chan<- *prometheus.Desc) {
	prometheus.DescribeByCollect(s, c)
}

func (s *BoschTemperature) Collect(c chan<- prometheus.Metric) {
	log.Info("Collecting Metrics for temperature sensor")
	for _, bd := range s.BoschDevices {
		boschTemperature := bosch.GetTemperature(bd.ID)
		c <- prometheus.MustNewConstMetric(s.Temperature, prometheus.GaugeValue, boschTemperature.Temperature, bd.ID, bd.Manufacturer, bd.RoomID, bd.Name, bd.RoomName)
	}
}
