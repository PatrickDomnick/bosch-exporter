package collector

import (
	"github.com/prometheus/client_golang/prometheus"
	log "github.com/sirupsen/logrus"
	"gitlab.com/PatrickDomnick/bosch-exporter/pkg/bosch"
)

type BoschDevice struct {
	Available *prometheus.Desc
	Rooms     []bosch.BoschRoom
}

func NewBoschDevice(rooms []bosch.BoschRoom) *BoschDevice {
	return &BoschDevice{
		Available: prometheus.NewDesc(deviceState, "available state for this device", deviceLabels, nil),
		Rooms:     rooms,
	}
}

func (s *BoschDevice) Describe(c chan<- *prometheus.Desc) {
	prometheus.DescribeByCollect(s, c)
}

func (s *BoschDevice) Collect(c chan<- prometheus.Metric) {
	log.Info("Collecting Metrics for all Devices")
	boschDevices := bosch.GetDevices(s.Rooms)
	for _, bd := range boschDevices {
		state := float64(0)
		if bd.Status == "AVAILABLE" {
			state = float64(1)
		}
		c <- prometheus.MustNewConstMetric(s.Available, prometheus.GaugeValue, state, bd.ID, bd.Manufacturer, bd.RoomID, bd.Name, bd.RoomName)
	}
}
