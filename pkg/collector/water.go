package collector

import (
	"github.com/prometheus/client_golang/prometheus"
	log "github.com/sirupsen/logrus"
	"gitlab.com/PatrickDomnick/bosch-exporter/pkg/bosch"
)

type BoschWater struct {
	BoschDevices []bosch.BoschDevice
	Leakaged     *prometheus.Desc
}

func NewBoschWater(boschDevices []bosch.BoschDevice) *BoschWater {
	return &BoschWater{
		BoschDevices: boschDevices,
		Leakaged:     prometheus.NewDesc(waterState, "leakaged water state", waterLabels, nil),
	}
}

func (s *BoschWater) Describe(c chan<- *prometheus.Desc) {
	prometheus.DescribeByCollect(s, c)
}

func (s *BoschWater) Collect(c chan<- prometheus.Metric) {
	log.Info("Collecting Metrics for this water")
	// Level
	for _, bd := range s.BoschDevices {
		boschWater := bosch.GetWaterState(bd.ID)
		// Operation State
		state := float64(1)
		if boschWater.Value == "NO_LEAKAGE" {
			state = float64(0)
		}
		c <- prometheus.MustNewConstMetric(s.Leakaged, prometheus.GaugeValue, state, bd.ID, bd.Manufacturer, bd.RoomID, bd.Name, bd.RoomName)
	}
}
