package collector

import (
	"github.com/prometheus/client_golang/prometheus"
	log "github.com/sirupsen/logrus"
	"gitlab.com/PatrickDomnick/bosch-exporter/pkg/bosch"
)

type BoschShutter struct {
	BoschDevices   []bosch.BoschDevice
	Level          *prometheus.Desc
	OperationState *prometheus.Desc
}

func NewBoschShutter(boschDevices []bosch.BoschDevice) *BoschShutter {
	return &BoschShutter{
		BoschDevices:   boschDevices,
		Level:          prometheus.NewDesc(shutterStateLevel, "percentage of this open shutter level", shutterLabels, nil),
		OperationState: prometheus.NewDesc(shutterStateOperation, "moving (1) or stopped (0) state state of this shutter", shutterLabels, nil),
	}
}

func (s *BoschShutter) Describe(c chan<- *prometheus.Desc) {
	prometheus.DescribeByCollect(s, c)
}

func (s *BoschShutter) Collect(c chan<- prometheus.Metric) {
	log.Info("Collecting Metrics for this shutter")
	// Level
	for _, bd := range s.BoschDevices {
		boschShutter := bosch.GetShutterState(bd.ID)
		c <- prometheus.MustNewConstMetric(s.Level, prometheus.GaugeValue, boschShutter.Level, bd.ID, bd.Manufacturer, bd.RoomID, bd.Name, bd.RoomName)
		// Operation State
		state := float64(0)
		if boschShutter.OperationState == "MOVING" {
			state = float64(1)
		}
		c <- prometheus.MustNewConstMetric(s.OperationState, prometheus.GaugeValue, state, bd.ID, bd.Manufacturer, bd.RoomID, bd.Name, bd.RoomName)
	}
}
